#!/bin/sh
set -ex
mkdir -p /dev/shm/ytdl
cd /dev/shm/ytdl
yt-dlp -f 'bestvideo[height<=360][ext=mp4]+bestaudio[ext=m4a]' "$(xsel -b)" --exec 'mpv --video-unscaled=yes'
