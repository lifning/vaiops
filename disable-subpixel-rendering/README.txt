This is stuff for disabling subpixel rendering of fonts which helps make text not look rainbowy when you arent running at the native resolution. you don't have to do it just go with whatever look you prefer.

the .conf files go in /etc/fonts/conf.d

make sure to delete any config files already there that mention anything about lcd filters or rgba or subpixel or any of that.

the Xresources stuff goes in ~/.Xresources or wherever and is for the programs that pay attention to that. you can change the hinting or antialias settings as you like tho the important thing is disabling lcdfilter and rgba

Also with a program like lxappearance you can disable it in the gtk settings too but idk if that actually matters but it might override the conf.d settings so might be good to do that too
