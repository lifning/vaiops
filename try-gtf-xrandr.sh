#!/bin/bash
set -eo pipefail

display=LVDS-1
modeline=($(gtf "$@" | grep Modeline | sed 's/"//g'))
mode=${modeline[1]}

set -x

xrandr --newmode ${modeline[@]:1}
if ! xrandr --addmode $display $mode ; then
	exec xrandr --rmmode $mode
fi
if ! xrandr --output $display --mode $mode ; then
	xrandr --delmode $display $mode
	exec xrandr --rmmode $mode
fi

gtf "$@"
